<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

/**
 * Class User
 * @package App\Models
 * @version July 10, 2020, 5:05 am UTC
 *
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $referral_by
 * @property string $no_of_refs
 * @property string $ref_level_id
 * @property string $role_id
 * @property string|\Carbon\Carbon $email_verified_at
 * @property string $remember_token
 */
class User extends Model
{

    public $table = 'users';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';




    public $fillable = [
        'name',
        'email',
        'password',
        'referral_by',
        'no_of_refs',
        'ref_level_id',
        'role_id',
        'email_verified_at',
        'remember_token'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'email' => 'string',
        'password' => 'string',
        'referral_by' => 'string',
        'no_of_refs' => 'string',
        'ref_level_id' => 'string',
        'role_id' => 'string',
        'email_verified_at' => 'datetime',
        'remember_token' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'email' => 'required',
        'password' => 'required'
    ];

    
}
