<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class RefLevel
 * @package App\Models
 * @version July 10, 2020, 10:11 am UTC
 *
 * @property string $name
 * @property string $description
 * @property string $reward
 * @property string $congratulatory_message
 * @property string $targets_no_referrals
 * @property string $points_per_referral
 */
class RefLevel extends Model
{
    use SoftDeletes;

    public $table = 'ref_levels';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'description',
        'reward',
        'congratulatory_message',
        'targets_no_referrals',
        'points_per_referral'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'description' => 'string',
        'reward' => 'string',
        'congratulatory_message' => 'string',
        'targets_no_referrals' => 'string',
        'points_per_referral' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required'
    ];

    
}
