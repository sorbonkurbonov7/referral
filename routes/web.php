<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->middleware('verified');

Route::resource('users', 'userController');

Route::resource('roles', 'RoleController');

Route::resource('refCategories', 'RefCategoryController');

Route::resource('refLevels', 'RefLevelController');

Route::get('refs/{user_id}/{ref_category_id?}', 'RefCategoryController@refs')->name('refs.user');
